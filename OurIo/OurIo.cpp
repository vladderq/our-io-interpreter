﻿// OurIo.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "resource.h"
#include "Scaner.h"

int main()
{
	char buff[BUFFSIZE];
	memset(buff, 0, BUFFSIZE);
	strcpy_s(buff, BUFFSIZE, "Examples/prog9_error/prog9.txt");

	// Создадим сканер
	Scaner s;
	// Загрузим состояния автомата
	s.read_state();
	// Просканируем файл
	s.scan_file(buff);
	// Получим таблицу имен 
	NameTable table = s.get_table();
	table.print();

	return 0;
}
