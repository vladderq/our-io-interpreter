g := (5 sqrt + 1) / 2;
for(i,1,16,
   N := ((g pow(i)) - ((1-g) pow(i))) / (5 sqrt);
   N round print;
   ", " print;
);
"..." println; 