#pragma once
#include "resource.h"
#include "NameNote.h"

// ����� ������� ����
class NameTable
{
private:
	// ������ �������
	list<NameNote> table;

public:
	// �����������
	NameTable() {
		this->table;
	}

	// �������� ������ � ��������� �������
	NameNote get_note(int n) {
		int i = 0;
		// c������� �������� �� ������ ������� ������
		auto it_table = this->table.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_table != this->table.end()) {
			if (i == n) {
				return *it_table;
			}
			++i;
			++it_table;
		}
		return -1;
	}

	// �������� ����� ������ � ��������� ��������
	int find_note(char* str) {
		int i = 0;
		// c������� �������� �� ������ ������� ������
		auto it_table = this->table.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_table != this->table.end()) {
			if (it_table->get_lexeme() == str) {
				return i;
			}
			++i;
			++it_table;
		}
		return -1;
	}

	// �������� ������ ������
	void add_note() {
		table.push_back(NameNote(table.size()));
	}

	// �������� ������ id
	int add_note_id(char* str) {
		int n = find_note(str);
		if (n != -1)
			return n;
		add_note();
		table.back().Identifier(str);
		return table.size() - 1;
	}

	// �������� ������ int
	int add_note_int(char* str) {
		int n = find_note(str);
		if (n != -1)
			return n;
		add_note();
		table.back().Integer(str);
		return table.size() - 1;
	}

	// �������� ������ str
	int add_note_str(char* str) {
		int n = find_note(str);
		if (n != -1)
			return n;
		add_note();
		table.back().String(str);
		return table.size() - 1;
	}

	// �������� ������ sign
	int add_note_sign(char* str) {
		int n = find_note(str);
		if (n != -1)
			return n;
		add_note();
		table.back().Sign(str);
		return table.size() - 1;
	}

	// ������ �������
	void print() {
		// c������� �������� �� ������ ������� ������
		auto it_table = this->table.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_table != this->table.end()) {
			it_table->print();
			++it_table;
		}
	}

	// ������ ������� � ����
	void fprint(char* filename) {
		FILE* file;
		fopen_s(&file, filename, "w");
		if (file == NULL) {
			printf("File isn't found\n");
			exit(3);
		}

		// c������� �������� �� ������ ������� ������
		auto it_table = this->table.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_table != this->table.end()) {
			it_table->fprint(file);
			++it_table;
		}
	}
};

