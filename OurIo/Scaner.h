#pragma once
#include "resource.h"
#include "NameTable.h"

// ����� �������
class Scaner
{
private:
	// ��������� ��������
	list<State> state;
	State now_state;

	// ������ ��������
	list<string> lexeme;
	list<string> symbols;
	
	// ������
	char* buffer;

	// ������� ����
	NameTable table;

	// ��������� ����������
	// ���������� ��� ��������� ��������� ��������� ��������
	bool ungetch;

	// �������� ��������
	map <string, int> mapping;

public:

	// �����������
	Scaner() {
		this->state;
		now_state;
		this->lexeme;
		this->symbols;
		table;
		ungetch = false;
		buffer = (char*)malloc(BUFFSIZE * sizeof(char));

		//��������� �������� ��������
		mapping["clear"] = 1;
		mapping["add"] = 2;
		mapping["ungetch"] = 3;
		mapping["write_id"] = 4;
		mapping["write_int"] = 5;
		mapping["write_str"] = 6;
		mapping["write_sign"] = 7;
	}

	// �������� ������� ����
	NameTable& get_table() {
		return table;
	}

	// ������������ �����
	void scan_file(char* filename) {

		// �������� ����
		FILE* file;
		fopen_s(&file, filename, "r");
		if (file == NULL) {
			printf("File isn't found\n");
			exit(3);
		}

		// �������������� ����
		FILE* outfile;
		fopen_s(&outfile, (char*)"lexemes.txt", "w");
		if (outfile == NULL) {
			printf("File isn't found\n");
			exit(3);
		}

		// ���������� ������� ��� ���������� ����� �� �����
		char* buff;
		buff = (char*)malloc(BUFFSIZE * sizeof(char));
		if (buff == NULL)
		{
			printf("Malloc error\n");
			exit(4);
		}

		// ������� ���������� ��������� ��������
		now_state = state.front();

		//����������� ������������
		while (!feof(file)) {
			if (fgets(buff, BUFFSIZE, file)) {
				scan_line(buff, outfile);
				fprintf(outfile, "\n");
			}
		}

		free(buff);
		// ��������� ������� ���� � ����
		table.fprint((char*)"table.txt");
	}

	// ������������ ������
	void scan_line(char *line, FILE* outfile) {
		// ��������� �� ������
		char* point = line;

		// ���������� ��� ��������� ������ � ���������
		string lex;
		string next;
		string action;
		int action_size = 0;
		
		// ���� �� ����� �� ����� ������
		while (*point != '\n' and *point != '\0') {
			ungetch = false;

			// ���� ��������� �������
			lex = find_lex(*point);
			if (lex == "no") {
				printf("Error no\n");
				exit(1);
			}

			// �������� ��������� ���������
			next = now_state.get_next(lex);
			if (next == "E") {
				printf("Unknown symbol: %c\n", *point);
				exit(100);
			}

			// ��������� �������� ��� �������� ���������
			action_size = now_state.get_action_size(lex);
			for (int i = 0; i < action_size; i++) {
				action = now_state.get_action(lex, i);
				do_action(action, *point, outfile);
			}

			// ������ ���������
			now_state = change_state(next);
			if (ungetch == false) {
				point++;
			}
		}

		// ������������ ����� ������
		lex = "newline";
		next = now_state.get_next(lex);
		action_size = now_state.get_action_size(lex);
		for (int i = 0; i < action_size; i++) {
			action = now_state.get_action(lex, i);
			do_action(action, *point, outfile);
		}
		now_state = change_state(next);
	}

	// ���� ��������� �������
	string find_lex(char c) {
		if (c == ' ')
			return "space";
		// c������� �������� �� ������ ������� ������
		auto it_lexeme = lexeme.begin();
		auto it_symbols = symbols.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_lexeme != lexeme.end()) {
			if (it_symbols->rfind(c) != std::string::npos)
				return *it_lexeme;
			++it_lexeme;
			++it_symbols;
		}
		return "else";
	}

	// ���� ����� ���������
	State change_state(string next)
	{
		// c������� �������� �� ������ ������� ������
		auto it_state = state.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_state != state.end()) {
			if (it_state->get_name() == next)
				return *it_state;
			++it_state;
		}
		printf("Error\n");
		exit(2);
	}

	// ���������� ��������
	void do_action(string action, char c, FILE* outfile)
	{
		int temp;
		switch (mapping[action]) {
			case 1:  
				// ������� �������
				*buffer = '\0';
				break;
			case 2:
				// ���������� ������� � ������
				strcat_s(buffer, BUFFSIZE, &c);
				break;
			case 3:
				// �� ��������� ��������� ������
				ungetch = true;
				break;
			case 4:
				// �������� ������ � ������� ���� 
				table.add_note_id(buffer);
				temp = table.find_note(buffer);
				fprintf(outfile, "%d ", temp);
				break;
			case 5:
				// �������� ������ � ������� ���� 
				table.add_note_int(buffer);
				temp = table.find_note(buffer);
				fprintf(outfile, "%d ", temp);
				break;
			case 6:
				// �������� ������ � ������� ���� 
				table.add_note_str(buffer);
				temp = table.find_note(buffer);
				fprintf(outfile, "%d ", temp);
				break;
			case 7:
				// �������� ������ � ������� ���� 
				table.add_note_sign(buffer);
				temp = table.find_note(buffer);
				fprintf(outfile, "%d ", temp);
				break;
			default:  
				break;
		}
	}

	// ������� ��������� �������� �� �����
	void read_state()
	{
		char* filename;
		// ���������� ����� �����
		filename = (char*)malloc(BUFFSIZE);
		strncpy_s(filename, BUFFSIZE, "scaner.txt", 10);

		// �������� �����
		FILE* IN_PUT;
		fopen_s(&IN_PUT, filename, "r");
		if(IN_PUT == NULL)
		{
			printf("File not found!\n");
			return;
		}

		// ���������� ��������
		char* point;
		char* buff;
		char* buff1;
		char* buff2;
		char* buff3;
		buff = (char*)malloc(BUFFSIZE * sizeof(char));
		buff1 = (char*)malloc(BUFFSIZE * sizeof(char));
		buff2 = (char*)malloc(BUFFSIZE * sizeof(char));
		buff3 = (char*)malloc(BUFFSIZE * sizeof(char));

		// Block 1
		// ����������� ���������
		while (true)
		{
			fgets(buff, BUFFSIZE, IN_PUT);
			if (buff == strstr(buff, "#"))
				break;
			if (buff == strstr(buff, "//"))
				continue;
			sscanf_s(buff, "%s %s", buff1, BUFFSIZE, buff2, BUFFSIZE);
			lexeme.push_back(buff1);
			symbols.push_back(buff2);
		}
		// Block 2
		// ��������� ���������� ��������
		while (true)
		{
			fgets(buff, BUFFSIZE, IN_PUT);
			if (buff == strstr(buff, "#"))
				break;
			if (buff == strstr(buff, "//"))
				continue;
			sscanf_s(buff, "%s", buff1, BUFFSIZE);
			state.push_back(State(buff1));
		}
		// Block 3(
		// ���������� ������ � ��������� ��������
		while (!feof(IN_PUT))
		{
			fgets(buff, BUFFSIZE, IN_PUT);
			
			if (buff == strstr(buff, "#"))
				break;
			if (buff == strstr(buff, "//"))
				continue;
			int temp = 0;

			// buff1 - �������� ���������
			// buff2 - ��� ���������
			// buff3 - ��������� ���������
			sscanf_s(buff, "%s %s %s%n", buff1, BUFFSIZE, buff2, BUFFSIZE, buff3, BUFFSIZE, &temp);
			
			// ������� ������ � ������ ���������
			// c������� �������� �� ������ ������� ������
			auto it_state = state.begin();
			// ���� �������� �� ����� �� ������� ������
			while (it_state != state.end()) {
				if (it_state->get_name() == buff1) {
					it_state->add(buff2, buff3);
					break;
				}
				++it_state;
			}

			// ���������� �������� ��� ������ ��������
			point = buff + temp;
			while (*point != '\n') {
				int res = sscanf_s(point, "%s%n", buff3, BUFFSIZE, &temp);
				if (res == -1)
					break;
				it_state->add_action(buff2, buff3);
				point = point + temp;
			}
		}

		// ������� ��������
		free(buff);
		free(buff1);
		free(buff2);
		free(buff3);
	}

	// ������ ����������� �����
	void print_lex()
	{
		// c������� �������� �� ������ ������� ������
		auto it_lexeme = lexeme.begin();
		auto it_symbols = symbols.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_lexeme != lexeme.end()) {
			printf("%s %s\n", (*it_lexeme).c_str(), (*it_symbols).c_str());
			++it_lexeme;
			++it_symbols;
		}
	}

	// ������ ��������� ��������� � ��������� ��������
	void print_state()
	{
		// c������� �������� �� ������ ������� ������
		auto it_state = state.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_state != state.end()) {
			(*it_state).print();
			++it_state;
		}
	}

	// ������ ��������� ��������� � ��������� ��������
	// � ����
	void fprint_state(char* filename)
	{
		FILE* file;
		fopen_s(&file, filename, "w");
		if (file == NULL) {
			printf("File isn't found\n");
			exit(3);
		}

		// c������� �������� �� ������ ������� ������
		auto it_state = state.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_state != state.end()) {
			(*it_state).fprint(file);
			++it_state;
		}
	}
};

