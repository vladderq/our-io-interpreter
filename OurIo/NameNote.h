#pragma once
#include "resource.h"

// ����� ������ � ������� ����
class NameNote
{
private:
	// �����
	int num;
	// �������
	string lexeme;
	// ��� (int/str)
	string type;
	// ��� (const/id/sign)
	string kind;

public:
	// �����������
	NameNote(int num) {
		this->num = num;
		this->lexeme = "unknown";
		this->type = "unknown";
		this->kind = "unknown";
	}

	// ����������/��������
	void Identifier(char* str) {
		this->lexeme = str;
		this->type = "unknown";
		this->kind = "id";
	}

	// ����� �����
	void Integer(char* str) {
		this->lexeme = str;
		this->type = "int";
		this->kind = "const";
	}

	// ������
	void String(char* str) {
		this->lexeme = str;
		this->type = "str";
		this->kind = "const";
	}

	// ����
	void Sign(char* str) {
		this->lexeme = str;
		this->type = "unknown";
		this->kind = "sign";
	}

	// �������� �����
	int get_num() {
		return num;
	}

	// �������� �������
	string get_lexeme() {
		return lexeme;
	}

	// �������� ���
	string get_type() {
		return type;
	}

	// �������� ���
	string get_kind() {
		return kind;
	}

	// ������ ������
	void print() {
		printf("%d %s %s %s\n", num, type.c_str(), kind.c_str(), lexeme.c_str());
	}

	// ������ ������ � ����
	void fprint(FILE* file) {
		fprintf(file, "%d %s %s %s\n", num, type.c_str(), kind.c_str(), lexeme.c_str());
	}
};

